# Pass the build number from
OUT 					 := app
VERSION 			 := $(shell git describe --always --long --dirty)

ifndef ENV
	ENV = development
endif

default: run

run:
	@NODE_ENV=${ENV} ./node_modules/.bin/webpack-dev-server --host localhost --port 8080

test:
	@npm test

deps:
	@npm install

clean:
	@rm -Rf build

.PHONY: default run test deps clean
