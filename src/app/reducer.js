import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

// Reducers
import safe from './hotel-safe/hotel_safe_reducer'

const appReducer = combineReducers({
  safe,
  routerReducer
})

const rootReducer = ( state, action ) => {
  if (action.type === 'RECEIVE_LOGOUT')  {
    state = undefined
  }
  
  return appReducer(state, action)
}

export default rootReducer