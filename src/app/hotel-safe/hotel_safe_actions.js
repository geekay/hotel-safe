import { INVALID, SET_PIN, UNLOCK_SAFE } from '../constants/constants'

export const setPin = ( pin ) => ({
    type: SET_PIN,
    pin: pin,
})

export const unlockSafe = () => ({
    type: UNLOCK_SAFE
})

export function handleSetPin(pin) {
    store.dispatch(setPin(pin))
}

export function handleUnlock() {
    store.dispatch(unlockSafe())
}