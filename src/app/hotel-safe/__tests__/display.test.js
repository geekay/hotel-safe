import React from 'react';
import { shallow } from 'enzyme';
import { Display } from '../components/display';
import { INVALID } from '../../constants';

describe(HotelSafeDisplay, () => {
  let wrapper;

  const mockProps = {
    currentPin: '1234',
    locked: false,
    error: ''
  };

  beforeAll(() => {
    wrapper = shallow(<Display {...mockProps} />);
  });

  it('should be unlocked', () => {
    const lock = wrapper.find('div.green');
    expect(lock).toHaveLength(1);
  });

  it('should display INVALID when pin is invalid', () => {
    wrapper.setProps({
      ...mockProps,
      ...{error: INVALID}
    });
    const display = wrapper.find('div.display');
    expect(display.props().value).toMatch(INVALID);
  });

  it('should be locked when set to locked', () => {
    wrapper.setProps({
      ...mockProps,
      ...{locked: true}
    })
    const lock = wrapper.find('div.red');
    expect(lock).toHaveLength(1);
  })
});