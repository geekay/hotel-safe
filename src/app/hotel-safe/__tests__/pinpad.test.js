import React from 'react';
import { shallow } from 'enzyme';
import { PinPad } from '../components/pinpad';

describe(PinPad, () => {
  let wrapper;

  const mockProps = {
    resetCurrentPin: jest.fn(),
    handleButton: jest.fn(),
    handleSubmit: jest.fn()
  };

  beforeAll(() => {
    wrapper = shallow(<PinPad {...mockProps} />);
    mockProps.resetCurrentPin.mockClear();
    mockProps.handleButton.mockClear();
    mockProps.handleSubmit.mockClear();
  });

  it('should have 10 safe buttons that invokes handleButton when clicked', () => {
    const buttons = wrapper.find('button.pin-number');
    expect(buttons).toHaveLength(10);
    buttons.forEach(b => {
      const mockHandleButton = jest.fn();
      wrapper.instance().handleButton = mockHandleButton;
      b.simulate('click');
      expect(mockHandleButton).toHaveBeenCalled();
    });
  });

  it('should have one submit button that invokes handleSubmit when clicked', () => {
    const button = wrapper.find('button.submit');
    expect(button).toHaveLength(1);
    button.simulate('click');
    expect(mockProps.submitSafePin).toHaveBeenCalled();
  });

  it('should have one clear button that invokes resetCurrentPin when clicked', () => {
    const button = wrapper.find('button.clear');
    expect(button).toHaveLength(1);
    button.simulate('click');
    expect(mockProps.resetCurrentPin).toHaveBeenCalled();
  });
});