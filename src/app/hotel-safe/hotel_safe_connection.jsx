import { connect } from 'react-redux'
import HotelSafeContainer from './hotel_safe_container'
import { handleSetInvalid, handleSetPin, handleUnlock } from './hotel_safe_actions'

const mapStateToProps = ({ safe }) => ({
  safe: safe,
})

const mapDispatchToProps = dispatch => ({
  handleSetInvalid: () => handleSetInvalid(),
  handleSetPin: (pin) => handleSetPin(pin),
  handleUnlock: () => handleUnlock()
})

export default connect(mapStateToProps, mapDispatchToProps)(HotelSafeContainer)