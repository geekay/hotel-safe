import React from 'react'

const PinPadButton = ({value, fn, className}) => (
    <button className={`${className} pinpad-button`} onClick={fn(value)}>
        { value }
    </button>
)

export default PinPadButton