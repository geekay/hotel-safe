import React from 'react'
import PinPadButton from './pinpad_button'

class PinPad extends React.Component {
  render() {
    let buttons = [ 1, 2, 3, 4, 5, 6, 7, 8, 9]
    let { handleButton, resetCurrentPin, handleSubmit } = this.props
    return (
      <div className="pinpad-wrapper">
        { buttons.map( value => {
          return (
            <PinPadButton
              className="pin-number"
              value={value} 
              key={value}
              fn={ () => handleButton(value) } />
          )
        })}
        <PinPadButton
          className="clear"
          value="CLR"
          fn={ () => resetCurrentPin} />
        <PinPadButton
          className="pin-number"
          value={0}
          fn={ () => handleButton(0)} />
        <PinPadButton
          className="submit"
          value=">"
          fn={ () => handleSubmit} />
      </div>
    )
  }
}

export default PinPad
