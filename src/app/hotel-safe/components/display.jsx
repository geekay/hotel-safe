import React from 'react'

class Display extends React.Component {
  render() {
    let { currentPin, locked, error } = this.props
    return (
      <div className="display-wrapper">
        <div className="display">
            { currentPin ? currentPin : error }
        </div>
        <div className={`circle ${ locked ? 'red' : 'green' }`} />
      </div>
    )
  }
}

export default Display
