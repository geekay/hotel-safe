import { INVALID, SET_PIN, UNLOCK_SAFE } from '../constants/constants'

const initialState = {
    pin: '',
    locked: false
}

const calculatorReducers = ( state = initialState, action ) => {
    let { 
        type,
        pin,
        locked
    } = action
    switch (type){
        // All of these cases can be handled in the same way
        // Shown how to handle them seperately
        case SET_PIN:
            return {...state, pin: pin, locked: true}
        case UNLOCK_SAFE:
            return {...state, pin: '', locked: false}
        default: 
            return {...state}

    }

}

export default calculatorReducers