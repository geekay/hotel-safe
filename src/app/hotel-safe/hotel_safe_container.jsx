import React from 'react'
import PinPad from './components/pinpad'
import Display from './components/display'
import { INVALID } from '../constants/constants'

class HotelSafeContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      safe: {},
      currentPin: "",
      error: ''
    }

    this.handleButton = this.handleButton.bind(this)
    this.resetCurrentPin = this.resetCurrentPin.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleLockedSubmit = this.handleLockedSubmit.bind(this)
    this.handleUnlockedSubmit = this.handleUnlockedSubmit.bind(this)
  }

  componentWillReceiveProps(nextProps){
    let { safe } = nextProps
    this.setState({ safe })
  }

  handleButton(value) {
    return () => {
      let { currentPin } = this.state
      if ( currentPin.length < 4 ) {
        currentPin += value
        this.setState({ currentPin: currentPin, error: '' })
      } 
    }
  }

  resetCurrentPin() {
      this.setState({ currentPin: '', error: '' })
  }

  handleSubmit() {
    let { safe } = this.state
    if ( !!safe.locked ) {
        this.handleLockedSubmit()
    } else { 
        this.handleUnlockedSubmit()
    }
  }

  handleLockedSubmit() { 
      let { safe, handleUnlock, handleSetInvalid } = this.props
      let { currentPin } = this.state
      if ( currentPin === safe.pin ) {
        this.resetCurrentPin()
        handleUnlock()
      } else {
        this.resetCurrentPin()
        this.handleError()
      }
  }

  handleUnlockedSubmit() {
      let { currentPin } = this.state
      let { handleSetInvalid, handleSetPin } = this.props
      if ( currentPin.length !== 4 ) {
        this.resetCurrentPin()
        this.handleError()
      } else { 
        this.resetCurrentPin()
        handleSetPin(currentPin)
      }
  }

  handleError() {
      this.setState({ error: INVALID }) 
  }

  render() {
    let { safe } = this.props
    let { locked } = safe ? safe : ''
    let { currentPin, error } = this.state
    return (
      <div className="hotel-safe">
        <div className="item-wrapper">
          <PinPad 
              resetCurrentPin={this.resetCurrentPin}
              handleButton={this.handleButton}
              handleSubmit={this.handleSubmit} />
          <Display 
              locked={locked}
              currentPin={currentPin}
              error={error}  />
        </div>
      </div>
    )
  }
}

export default HotelSafeContainer
