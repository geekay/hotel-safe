# Hotel Safe

#### Steps

1. Install dependencies:
`make deps`

2. Start the webpack dev server:
`make`

3. Run the tests:
`make test`