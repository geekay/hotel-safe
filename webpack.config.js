var HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var webpack = require('webpack');
var autoprefixer = require('autoprefixer');
var babelpolyfill = require('babel-polyfill');
var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
var CompressionPlugin = require("compression-webpack-plugin");
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');


var config = {
  entry: [ "./src/app/entry.jsx", "babel-polyfill" ], 
  output: {
    path: __dirname + '/build',
    filename: "bundle.js",
    publicPath: '/'
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV)
      }
    }),
    new webpack.LoaderOptionsPlugin({
      options: {
        postcss: [
          autoprefixer({
            browsers: ['last 2 version']
          })
        ]
      }
    }),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      filename: 'index.html',
      inject: 'head'
    }),
  ],
  module: {
    rules: [
      {
        test: [/\.jsx?$/, /\.js?$/],
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['es2015', 'react', 'stage-2'],
            cacheDirectory: true
          }
        },
      },
      {
        test: /\.css$/,
        loaders: ['style-loader', 'css-loader', 'postcss-loader' ]
      },
      {
        test: /\.scss$/,
        loaders: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader']
      },
      {
        test: [/\.woff?$/, /\.woff2?$/, /\.ttf?$/, /\.eot?$/, /\.svg?$/, /\.otf?$/],
        use: { loader: 'url-loader' }
      }
    ]
  },
  devtool: 'source-maps',
  resolve: { extensions: [".js", ".jsx" ] },
  externals: {
    'cheerio': 'window',
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true,
  },
  devServer: {
    historyApiFallback: {
      rewrites: [{
        from: /\/(\d\.)?app\.js(\.map)?/,
        to: context => context.match[0]
      }]
    }
  }
};

module.exports = config;
